/***************
 * JS-NeuralNet
 * Created by Sean Lander (c) 2012, All Rights Reserved
 
   This file is part of JS-NeuralNet.

   JS-NeuralNet is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   JS-NeuralNet is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with JS-NeuralNet.  If not, see <http://www.gnu.org/licenses/>.
 ***************/
 
/***************
 * JSNN Properties:
 *  training_data: Training Data to use
 *  input_count: Number of Input Nodes
 *  hidden_count: Number of Hidden Nodes
 *  hidden_activation: Activation Function for Hidden Layer
 *  hidden_weights_randomize: Do we randomize the weights on start or use default (0.1)
 *  output_count: Number of Output Nodes
 *  output_activation: Activation Function for Output Layer
 *	output_weights_randomize: Do we randomize the weights on start or use default (0.1)
 *  training_pct: % of training data contained in training set
 *  validation_pct: % of training data contained in validation set
 *  testing_pct: % of training data contained in testing set
 *  alpha: Learning Rate
 *  beta: Momentum Term
 *  target_error: Target Error
 *  validation_count: Number of increases in error before
 *    assumed convergence
 *  max_epochs: Maximum Epochs before cutoff
 *	callback: Function to be called at the end of each epoch
 *
 * JSNN Objects:
 *	mlp: dictionary of multi-layer-perceptrons
 *	training_set: subset of training_data to be used for training
 *	validation_set: subset of training_data to be used for validation/learning cuttoff
 *	testing_set: subset of training_data to be used for final confusion graph
 *	classes: dictionary containing class number and its associated desired output
 *	err: error handling object
 *
 * JSNN Functions:
 *	default: sets default perceptron properties
 *	reset: resets perceptron properties to zero'd out values
 *	init: takes in values from external input and sets up the JSNN
 *	run: runs the JSNN with the current values
 *	splitData: splits the training data into a training set, validation set and testing set
 ***************/
 

var JSNN = JSNN || {};

// Initilize Arrays and Dictionaries
JSNN.mlp = {};
JSNN.training_set = [];
JSNN.validation_set = [];
JSNN.testing_set = [];
JSNN.classes = {};

// Set up Error System
JSNN.err = JSNN.err || {};
JSNN.err.no = 0;
JSNN.err.msg = function() {
	switch(JSNN.err.no) {
		case 0:
			"No Error";
			break;
		case 1:
			"Error Initializing";
			break;
		default:
			"Unknown Error";
	}
};
JSNN.err.details = [];


/***************
 * Init:
 * Inputs:
 *	all setable values for the JSNN
 * Output:
 *	success: 0
 *  failure: -1
 ***************/
JSNN.init = function(training_data,
	hidden_count,
	hidden_activation,
	hidden_weights_randomize,
	output_activation,
	output_weights_randomize,
	training_pct,
	validation_pct,
	testing_pct,
	alpha,
	beta,
	target_error,
	validation_count,
	max_epochs) {
	// Reset Errors
	this.err.no = 0;
	this.err.details = [];

	// Verify training data exists and is valid
	if(training_data == null ||
		training_sets[training_data] == null ||
		training_sets[training_data].data == null ||
		training_sets[training_data].data.length <= 0) {
		this.err.no = 1;
		this.err.details.push("Training Set not valid");
	} else {
		this.training_data = training_sets[training_data];
	}
	
	// Calculate Inputs from Training Data
	this.input_count = this.training_data.data[0].length-1;

	// Verify Hidden Count is Valid
	if(!isInt(hidden_count) || hidden_count <= 0) {
		this.err.no = 1;
		this.err.details.push("Invalid Hidden Count. Must be integer greater than 0");
	} else {
		hidden_count = parseInt(hidden_count);
		this.hidden_count = hidden_count;
	}

	// Verify Hidden Activation Function is Valid
	if(Activation[hidden_activation] == undefined) {
		this.err.no = 1;
		this.err.details.push("Unknown Activation Function "+hidden_activation+" selected for Hidden Layer");
	} else {
		this.hidden_activation = hidden_activation;
	}

	// Check if Hidden Weights should be randomized
	if( ! (hidden_weights_randomize === true || hidden_weights_randomize === false) ) {
		this.err.no = 1;
		this.err.details.push("Hidden Weights Randomize should be True or False only");
	} else {
		this.hidden_weights_randomize = hidden_weights_randomize;
	}
	
	// Calculate Output Count from Training Data
	for( var i = 0 ; i < this.training_data.data.length ; i++ ) {
		this.classes[this.training_data.data[i].slice(-1)] = [];
	}
	this.output_count = 0;
	for( var k in this.classes ) { this.output_count++; };

	// Set up Class Patterns
	var classnum = 0;
	var top = ( this.output_activation == 'sigmoid' ? 0.95 : 0.9 );
	var bottom = ( this.output_activation == 'sigmoid' ? 0.05 : -0.9 );
	for( var d in this.classes ) {
		for( var i = 0 ; i < this.output_count ; i++ ) {
			this.classes[d][i] = ( i == classnum ? top : bottom );
		}
		classnum++;
	}

	// Verify Output Activation Function
	if(Activation[output_activation] == undefined) {
		this.err.no = 1;
		this.err.details.push("Unknown Activation Function "+hidden_activation+" selected for Output Layer");
	} else {
		this.output_activation = output_activation;
	}
	
	// Check if Output Weights should be randomized
	if( ! (output_weights_randomize === true || output_weights_randomize === false) ) {
		this.err.no = 1;
		this.err.details.push("Output Weights Randomize should be True or False only");
	} else {
		this.output_weights_randomize = output_weights_randomize;
	}

	// Check if Training Percentage is Valid
	if(training_pct < 0 || 1 < training_pct) {
		this.err.no = 1;
		this.err.details.push("Training Percent must be between 0 and 1");
	} else {
		training_pct = parseFloat(training_pct);
		this.training_pct = training_pct;
	}

	// Check if Validation Percentage is Valid
	if(validation_pct < 0 || 1 < validation_pct) {
		this.err.no = 1;
		this.err.details.push("Validation Percent must be between 0 and 1");
	} else {
		validation_pct = parseFloat(validation_pct);
		this.validation_pct = validation_pct;
	}

	// Check if Testing Percentage is Valid
	if(testing_pct < 0 || 1 < testing_pct) {
		this.err.no = 1;
		this.err.details.push("Test Percent must be between 0 and 1");
	} else {
		testing_pct = parseFloat(testing_pct);
		this.testing_pct = testing_pct;
	}

	// Sum the percentages and verify they sum to 100%
	if( (this.training_pct + this.validation_pct + this.testing_pct) != 1 ) {
		this.err.no = 1;
		this.err.details.push("All Percentages should add up to 1");
	}

	// Verify Alpha is a number
	if(isNaN(alpha)) {
		this.err.no = 1;
		this.err.details.push("Alpha needs to be a number");
	} else {
		alpha = parseFloat(alpha);
		this.alpha = alpha;
	}

	// Verify Beta is a number
	if(isNaN(beta)) {
		this.err.no = 1;
		this.err.details.push("Beta needs to be a number");
	} else {
		beta = parseFloat(beta);
		this.beta = beta;
	}
	
	// Verify Target Error is a number between 0 and 1
	if(isNaN(target_error) || target_error < 0 || 1 < target_error) {
		this.err.no = 1;
		this.err.details.push("Target Error must be a number between 0 and 1");
	} else {
		target_error = parseFloat(target_error);
		this.target_error = target_error;
	}

	// Verify Validation Count is a number greater than 0
	if(!isInt(validation_count) || validation_count <= 0) {
		this.err.no = 1;
		this.err.details.push("Validation Count must be a non-negative integer")
	} else {
		validation_count = parseInt(validation_count);
		this.validation_count = validation_count;
	}

	// Verify Max Epochs is a number greater than 0
	if(!isInt(max_epochs) || max_epochs <= 0) {
		this.err.no = 1;
		this.err.details.push("Max Epochs must be a non-negative integer")
	} else {
		max_epochs = parseInt(max_epochs);
		this.max_epochs = max_epochs;
	}
	
	// If there was an error, cancel out and print errors
	if(this.err.no != 0)
		return -1; // Error was found
	
	// Otherwise, Create/Reset the MLPs
	this.resetMLPs();
		
	return 0;
	
}
	
JSNN.resetMLPs = function() {
	// Create a new Multi Layer Perceptron with the new information
	this.mlp = new MLP();
	
	// Initialize the current MLP to be used
	this.mlp.init(this.input_count,
		this.hidden_count,
		this.hidden_activation,
		this.hidden_weights_randomize,
		this.output_count,
		this.output_activation,
		this.output_weights_randomize,
		this.alpha,
		this.beta);
		
	return this;
};


/***************
 * Defaults:
 *	Inputs:
 *		none
 *	Output:
 *		self
 ***************/
JSNN.defaults = function() {
	// Set Default Values
	this.training_data = training_sets['XOR'];
	this.hidden_count = 10;
	this.hidden_activation = "hypertangent";
	this.hidden_weights_randomize = true;
	this.classes = {1: [0.9, -0.9], 2: [-0.9, 0.9]};
	this.output_activation = "hypertangent";
	this.output_weights_randomize = true;
	this.training_pct = 0.7;
	this.validation_pct = 0.15;
	this.testing_pct = 0.15;
	this.alpha = 0.01;
	this.beta = 0.9;
	this.target_error = 0.001;
	this.validation_count = 5;
	this.max_epochs = 100;
	
	// Reset Errors
	this.err.no = 0;
	this.err.details = [];
	
	// Reset the MLPs
	this.resetMLPs();
	
	return this;
};


/***************
 * Reset:
 *	Inputs:
 *		none
 *	Output:
 *		self
 ***************/
JSNN.reset = function() {
	// Reset Values
	this.training_data = training_sets['XOR'];
	this.input_count = 0;
	this.hidden_count = 0;
	this.hidden_activation = null;
	this.hidden_weights_randomize = false;
	this.output_count = 0;
	this.classes = {};
	this.output_activation = null;
	this.output_weights_randomize = false;
	this.training_pct = 0;
	this.validation_pct = 0;
	this.testing_pct = 0;
	this.alpha = 0;
	this.beta = 0;
	this.target_error = 0;
	this.validation_count = 0;
	this.max_epochs = 0;
	
	// Reset Errors
	this.err.no = 0;
	this.err.details = [];
	
	// Reset the MLPs
	this.resetMLPs();
	
	return this;
};


/***************
 * Run:
 *	Runs the JSNN, initializing values, building a network, splitting the training_data
 *	then running epochs until one of the convergence conditions is met
 *	Inputs:
 *		update(epoch, sumsquarederror): Update callback. Takes epoch and sumsquarederror as arguments
 *		complete(mlp.best, epoch, sumsquarederror, confusion_matrix, count): Complete callback
 *	Output:
 *		self
 ***************/
JSNN.run = function(update, complete) {
	/***************
	 * Split up the Data
	 ***************/
	this.splitData();
	
	/***************
	 * Initiate values
	 ***************/
	var index = squarederror = 0;
	var line = input = desiredpattern = desiredclass = null;
	
	var training_set = [];
	var training_set_length = this.training_set.length;
	var validation_set = [];
	var validation_set_length = this.validation_set.length;
	var testing_set = [];
	var testing_set_length = this.testing_set.length;
	
	var epoch = 1; // Reset the Epoch to 1
	var validation_error_increased = 0; // Keep track of how many times the validation error has increased
	
	// Set initial current error for cases where target error is valid
	var sumsquarederror = {}; 
	sumsquarederror.training = 100;
	sumsquarederror.validation = 0;
	sumsquarederror.pvalidation = 0;
	sumsquarederror.testing = 0;
	
	// Create Cloned MLP and default Best dictionary
	var best = {};
	best.mlp = jQuery.extend(true, {}, this.mlp);
	best.epoch = epoch;

	/***************
	 * Loop until convergence
	 ***************/
	while( epoch < this.max_epochs+1 &&
		validation_error_increased < this.validation_count &&
		this.target_error < sumsquarederror.training) {
		
		// Zero-out the current error
		sumsquarederror.training = 0;
		sumsquarederror.pvalidation = sumsquarederror.validation;
		sumsquarederror.validation = 0;
		sumsquarederror.testing = 0;
		
		// Loop through each pattern, sending it the inputs, the desired class, and whether it is a validation set or not
		for( var k = 0 ; k < validation_set_length ; k++ ) {
			index = Math.floor(Math.random()*this.validation_set.length); // Select a random element from the validation set
			line = this.validation_set.splice(index,1)[0]; // Grab that element
			
			validation_set.push(line); // Push it onto a new array to be used next loop
			input = line.slice(0,this.input_count); // Grab the Input
			desiredpattern = this.classes[line.slice(-1)[0]]; // Grab the Desired Output
			
			// Run input through the network
			this.mlp.run(input, desiredpattern, true);
			
			// Get the squared error
			squarederror = this.mlp.getSquaredError();
			
			// Sum the sqared error and divide it by the number of patterns
			sumsquarederror.validation += squarederror*(1/validation_set_length);
		}
		this.validation_set = validation_set; // Now recycle the validation sets since you chopped the original to bits
		
		// Loop through each pattern, sending it the inputs, the desired class, and whether it is a testing set or not
		for( var k = 0 ; k < testing_set_length ; k++ ) {
			index = Math.floor(Math.random()*this.testing_set.length); // Select a random element from the testing set
			line = this.testing_set.splice(index,1)[0]; // Grab that element
			
			testing_set.push(line); // Push it onto a new array to be used next loop
			input = line.slice(0,this.input_count); // Grab the Input
			desiredpattern = this.classes[line.slice(-1)[0]]; // Grab the Desired Output
			
			// Run input through the network
			this.mlp.run(input, desiredpattern, true);
			
			// Get the squared error
			squarederror = this.mlp.getSquaredError();
			
			// Sum the sqared error and divide it by the number of patterns
			sumsquarederror.testing += squarederror*(1/testing_set_length);
		}
		this.testing_set = testing_set; // Now recycle the testing sets since you chopped the original to bits
		
		// Test if validation error has decreased or started going up
		if( sumsquarederror.validation > sumsquarederror.pvalidation ) {
			validation_error_increased++;
		} else {
			validation_error_increased = 0;
			best.mlp = jQuery.extend(true, {}, this.mlp);
			best.epoch = epoch;
		}
		
		// Loop through each pattern, sending it the inputs, the desired class, and whether it is a training set or not
		// Leave Training Set for last so the updates happen only after a validation pass has happened
		for( var k = 0 ; k < training_set_length ; k++ ) {
			index = Math.floor(Math.random()*this.training_set.length); // Select a random element from the training set
			line = this.training_set.splice(index,1)[0]; // Grab that element
			
			training_set.push(line); // Push it onto a new array to be used next loop
			input = line.slice(0,this.input_count); // Grab the Input
			desiredpattern = this.classes[line.slice(-1)[0]]; // Grab the Desired Output
			// Run input through the network
			this.mlp.run(input, desiredpattern, true);
			
			// Get the squared error
			squarederror = this.mlp.getSquaredError();
			
			if( isNaN(squarederror) || !isFinite(squarederror) ) { console.log("At loop "+k+": "+squarederror); return null; }
			
			// Sum the sqared error and divide it by the number of patterns
			sumsquarederror.training += squarederror*(1/training_set_length);
		}
		this.training_set = training_set; // Now recycle the training sets since you chopped the original to bits
		
		// Send the Update callback the sumsquarederror information
		update(epoch, jQuery.extend({},sumsquarederror)); // Send the error back to the calling program
		
		// Increase epoch counter
		epoch++;
	}
	
	
	/***************
	 * Single pass for final data
	 ***************/
	
	// Initialize final output object
	best.sumsquarederror = {};
	best.sumsquarederror.training = 0;
	best.sumsquarederror.validation = 0;
	best.sumsquarederror.testing = 0;
	best.confusion_matrix = {};
	best.confusion_matrix.training = {};
	best.confusion_matrix.validation = {};
	best.confusion_matrix.testing = {};
	best.confusion_matrix.all = {};
	for( var received in this.classes ) {
		best.confusion_matrix.training[received] = {};
		best.confusion_matrix.validation[received] = {};
		best.confusion_matrix.testing[received] = {};
		best.confusion_matrix.all[received] = {};
		for( var desired in this.classes ) {
			best.confusion_matrix.training[received][desired] = 0;
			best.confusion_matrix.validation[received][desired] = 0;
			best.confusion_matrix.testing[received][desired] = 0;
			best.confusion_matrix.all[received][desired] = 0;
		}
	}
	
	// Set up pattern counts for confusion matrix
	var count = {};
	count.training = training_set_length;
	count.validation = validation_set_length;
	count.testing = testing_set_length;
	count.all = count.training+count.validation+count.testing;
	
	// Loop through each pattern, sending it the inputs, the desired class, and whether it is a validation set or not
	for( var k = 0 ; k < validation_set_length ; k++ ) {
		index = Math.floor(Math.random()*this.validation_set.length); // Select a random element from the validation set
		line = this.validation_set.splice(index,1)[0]; // Grab that element
		
		validation_set.push(line); // Push it onto a new array to be used next loop
		input = line.slice(0,this.input_count); // Grab the Input
		desiredpattern = this.classes[line.slice(-1)[0]]; // Grab the Desired Output
		desiredclass = line.slice(-1)[0]; // Get the key for the desired Output
		
		// Run input through the network
		best.mlp.run(input, desiredpattern, true);
		
		// Get the squared error
		squarederror = best.mlp.getSquaredError();
		
		// Update the confusion matricse
		best.mlp.computeClassification(input, this.classes);
		best.confusion_matrix.validation[best.mlp.getClassification()][desiredclass]++;
		best.confusion_matrix.all[best.mlp.getClassification()][desiredclass]++;
		
		// Sum the sqared error and divide it by the number of patterns
		best.sumsquarederror.validation += squarederror*(1/validation_set_length);
	}
	
	// Loop through each pattern, sending it the inputs, the desired class, and whether it is a testing set or not
	for( var k = 0 ; k < testing_set_length ; k++ ) {
		index = Math.floor(Math.random()*this.testing_set.length); // Select a random element from the testing set
		line = this.testing_set.splice(index,1)[0]; // Grab that element
		
		testing_set.push(line); // Push it onto a new array to be used next loop
		input = line.slice(0,this.input_count); // Grab the Input
		desiredpattern = this.classes[line.slice(-1)[0]]; // Grab the Desired Output
		desiredclass = line.slice(-1)[0]; // Get the key for the desired Output
		
		// Run input through the network
		best.mlp.run(input, desiredpattern, true);
		
		// Get the squared error
		squarederror = best.mlp.getSquaredError();
		
		// Update the confusion matricse
		best.mlp.computeClassification(input, this.classes);
		best.confusion_matrix.testing[best.mlp.getClassification()][desiredclass]++;
		best.confusion_matrix.all[best.mlp.getClassification()][desiredclass]++;
		
		// Sum the sqared error and divide it by the number of patterns
		best.sumsquarederror.testing += squarederror*(1/testing_set_length);
	}
	
	// Loop through each pattern, sending it the inputs, the desired class, and whether it is a training set or not
	// Leave Training Set for last so the updates happen only after a validation pass has happened
	for( var k = 0 ; k < training_set_length ; k++ ) {
		index = Math.floor(Math.random()*this.training_set.length); // Select a random element from the training set
		line = this.training_set.splice(index,1)[0]; // Grab that element
		
		training_set.push(line); // Push it onto a new array to be used next loop
		input = line.slice(0,this.input_count); // Grab the Input
		desiredpattern = this.classes[line.slice(-1)[0]]; // Grab the Desired Output
		desiredclass = line.slice(-1)[0]; // Get the key for the desired Output
		
		// Run input through the network
		best.mlp.run(input, desiredpattern, false);
		
		// Get the squared error
		squarederror = best.mlp.getSquaredError();
		
		if( isNaN(squarederror) || !isFinite(squarederror) ) { console.log("At loop "+k+": "+squarederror); return null; }
		
		// Update the confusion matricse
		best.mlp.computeClassification(input, this.classes);
		best.confusion_matrix.training[best.mlp.getClassification()][desiredclass]++;
		best.confusion_matrix.all[best.mlp.getClassification()][desiredclass]++;
		
		// Sum the sqared error and divide it by the number of patterns
		best.sumsquarederror.training += squarederror*(1/training_set_length);
	}
	
	// Send the Complete callback the best MLP, Epoch, Sumsquarederror and Conufsion Matrix to print out
	complete(best.mlp, best.epoch, best.sumsquarederror, best.confusion_matrix, count);
	
	return self;
};


/***************
 * SplitData:
 *	Randomly splits the training data into three sets: training_set,
 *	validation_set and testing_set
 * Inputs:
 *	none
 * Output:
 *	self
 ***************/
JSNN.splitData = function() {
	// Calculate the number of inputs in each set
	var training_num = Math.floor(this.training_data.data.length*this.training_pct);
	var validation_num = Math.floor(this.training_data.data.length*this.validation_pct);
	var testing_num = this.training_data.data.length-training_num-validation_num;

	// Init the training, validation and test sets
	this.training_set = [];
	this.validation_set = [];
	this.testing_set = [];
	var index = 0;

	var data_set = [];
	for( var i = 0 ; i < this.training_data.data.length ; i++ ) {
		data_set[i] = this.training_data.data[i];
	}

	// Create the training set
	for( var i = 0 ; i < training_num ; i++ ) {
		index = Math.floor(Math.random()*data_set.length);
		this.training_set[i] = data_set.splice(index,1)[0];
	}

	// Create the validation set
	for( var i = 0 ; i < validation_num ; i++ ) {
		index = Math.floor(Math.random()*data_set.length);
		this.validation_set[i] = data_set.splice(index,1)[0];
	}

	// Create the test set
	for( var i = 0 ; i < testing_num ; i++ ) {
		index = Math.floor(Math.random()*data_set.length);
		this.testing_set[i] = data_set.splice(index,1)[0];
	}
	
	return self;
}

/***************
 * isInt Function
 * Code found from http://www.inventpartners.com/javascript_is_int
 * Implemented by me
 ***************/

function isInt(x) {
	return ( (!isNaN(x) && parseFloat(x) == parseInt(x)) ? true : false );
};

/* Created with csv_to_js (c) Sean Lander */
/* Included as default data set */

/* Creating Data object XOR */
var training_sets = training_sets || {};

training_sets["XOR"] = {};
training_sets["XOR"].data = [
[0,1,1],		
[1,0,1],		
[0,0,2],		
[1,1,2]		
];

