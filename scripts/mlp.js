/***************
 * JS-NeuralNet
 * Created by Sean Lander (c) 2012, All Rights Reserved
 
   This file is part of JS-NeuralNet.

   JS-NeuralNet is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   JS-NeuralNet is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with JS-NeuralNet.  If not, see <http://www.gnu.org/licenses/>.
 ***************/

/***************
 * MLP Properties:
 *  input_count: Number of Input Nodes
 *  hidden_count: Number of Hidden Nodes
 *  hidden_activation: Activation Function for Hidden Layer
 *  hidden_weights_randomize: Do we randomize the weights on start or use default (0.1)
 *  output_count: Number of Output Nodes
 *  output_activation: Activation Function for Output Layer
 *	output_weights_randomize: Do we randomize the weights on start or use default (0.1)
 *  alpha: Learning Rate
 *  beta: Momentum Term
 *	errors: Array of errors for last past
 *	squarederror: Squared error for last pass
 *	classification: Nearest approximation of class for last pass
 *
 * MLP Objects:
 *	layers: Dictionary of layers in the MLP (for future expansion beyond single hidden)
 *
 * MLP Functions:
 *	init: Takes in values from external input and sets up the MLP
 *	buildNetwork: Builds a fresh MLP using the current values
 *	run: Runs the MLP with the current values
 *	forward: Runs a forward pass on the MLP using the specified set and pattern
 *	computeErrors: Computes the error array based on the desired output pattern
 *	getErrors: Returns the MLP's most recent error array
 *	computeSquaredError: Computes the squared error using the most recent error array
 *	getSquaredError: Returns the most recent squared error
 *	computeClassification
 *	getClassification
 *	backward: Backpropogates the error from the forward pass, updating the weights
 ***************/

/***************
 * Constructor:
 *	Inputs:
 *		none
 *	Output:
 *		self
 ***************/
function MLP() {
	// Initilize Arrays
	this.layers = {};
};


/***************
 * Init:
 * Inputs:
 *	all setable values for the MLP
 * Output:
 *	success: 0
 *  failure: -1
 ***************/
MLP.prototype.init = function(input_count,
	hidden_count,
	hidden_activation,
	hidden_weights_randomize,
	output_count,
	output_activation,
	output_weights_randomize,
	alpha,
	beta) {

	this.input_count = input_count;
	this.hidden_count = hidden_count;
	this.hidden_activation = Activation[hidden_activation];
	this.hidden_weights_randomize = hidden_weights_randomize;
	this.output_count = output_count;
	this.output_activation = Activation[output_activation];
	this.output_weights_randomize = output_weights_randomize;
	this.alpha = alpha;
	this.beta = beta;
	
	this.buildNetwork();
	
	return self;
};


/***************
 * BuildNetwork:
 *	Creates the layers for the network, each of which contains
 *	an array of perceptrons which are fed data.
 *	Creates the class patterns based on the number of outputs.
 * Inputs:
 *	none
 * Output:
 *	self
 ***************/
MLP.prototype.buildNetwork = function() {
	// Create the hidden and output layers
	this.layers.hidden = new Layer(this.hidden_count,
																	this.input_count,
																	this.randomize_weights,
																	this.default_weight,
																	this.hidden_activation,
																	this.alpha,
																	this.beta,
																	true);
	this.layers.output = new Layer(this.output_count,
																	this.hidden_count,
																	this.randomize_weights,
																	this.default_weight,
																	this.output_activation,
																	this.alpha,
																	this.beta,
																	false);
	return self;
}


/***************
 * Run:
 *	Runs the MLP using the current dataset.
 *	Inputs:
 *		dataset: dataset to be used to gather sum of squared error
 *		training: true/false if this is a training set or not
 *	Output:
 *		self
 ***************/
MLP.prototype.run = function(input, desired, training) {
	// Compute the actual squared error for the network
	this.forward(input).computeErrors(desired).computeSquaredError();
	
	// Compute the backpropagation if necessary
	if( training ) {
		this.backprop(input);
	}
	
	return this;
};

 
/***************
 * Forward:
 *	Runs a forward pass through the MLP given the 
 * Inputs:
 *	none
 * Output:
 *	self
 ***************/
MLP.prototype.forward = function(input) {
	// Run the inputs through the layers
	this.layers.hidden.run(input);
	this.layers.output.run(this.layers.hidden.getOutputs());
	
	return this;
};


/***************
 * ComputeSquaredErrors:
 *	Computes the Squared Error
 *	Inputs:
 *		none
 *	Output:
 *		self
 ***************/
MLP.prototype.computeSquaredError = function() {
	this.squarederror = 0;
	for( var i = 0 ; i < this.errors.length ; i++ ) {
		this.squarederror += this.errors[i]*this.errors[i]*0.5;
	}
	return this;
};


/***************
 * GetSquaredError:
 *	Returns the MLP's most recent squared error
 *	Inputs:
 *		none
 *	Output:
 *		squarederror
 ***************/
MLP.prototype.getSquaredError = function() {
	return this.squarederror;
};


/***************
 * ComputeErrors:
 *	Computes the most recent error array
 *	Inputs:
 *		none
 *	Output:
 *		self
 ***************/
MLP.prototype.computeErrors = function(desired) {
	// Now grab the outputs for the final layer and return the error array
	this.errors = [];
	this.errors = this.layers.output.calculateErrors(desired).getErrors();
	return this;
};


/***************
 * GetErrors:
 *	Returns the MLP's most recent error array
 *	Inputs:
 *		none
 *	Output:
 *		errors
 ***************/
MLP.prototype.getErrors = function() {
	return jQuery.extend([], this.errors);
};


/***************
 * ComputeClassification:
 *	Returns the MLP's most recent best fit classification
 *	Inputs:
 *		input
 *		classifications
 *	Output:
 *		self
 ***************/
MLP.prototype.computeClassification = function(input, classifications) {
	this.classification = undefined;
	var besterror = 1000;
	for( var c in classifications ) {
		this.forward(input).computeErrors(classifications[c]).computeSquaredError();
		if( this.getSquaredError() < besterror ) {
			besterror = this.getSquaredError();
			this.classification = c;
		}
	}
	return this;
};


/***************
 * GetClassification:
 *	Returns the MLP's most recent best fit classification
 *	Inputs:
 *		none
 *	Output:
 *		classification
 ***************/
MLP.prototype.getClassification = function() {
	return this.classification;
};

 
/***************
 * Backprop:
 *	Computes the back propagation of the network 
 * Inputs:
 *	none
 * Output:
 *	self
 ***************/
MLP.prototype.backprop = function(input) {
	// Calculate the gradients and the weight deltas from that for each layer
	this.layers.output.calculateGradients().calculateDWeights(this.layers.hidden.getOutputs());
	this.layers.hidden.calculateGradients(this.layers.output).calculateDWeights(input);
	
	// Apply the weight deltas after they have all been computated
	this.layers.hidden.applyDWeights();
	this.layers.output.applyDWeights();
	
	return self;
};
