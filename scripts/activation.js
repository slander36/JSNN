/***************
 * JS-NeuralNet
 * Created by Sean Lander (c) 2012, All Rights Reserved
 
   This file is part of JS-NeuralNet.

   JS-NeuralNet is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   JS-NeuralNet is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with JS-NeuralNet.  If not, see <http://www.gnu.org/licenses/>.
 ***************/
 
 /***************
 * Activation Dictionary:
 *	This object contains a dictionary of commonly used activation functions
 *	for use with perceptron.
 *	This is a subset of the larger JS-Perceptron project but can be used
 *	standalone as well.
 ***************/

var Activation = Activation || {};

// Sigmoid Function
Activation.sigmoid = function(v, a) {
	if( !a ) { a = 1 }
	var small = Math.exp(-a * v);
	small = ( isFinite(small) ? small : Math.pow(10,100) );
	return ( 1 / ( 1 + Math.exp(-a * v) ) );
};
Activation.sigmoid.delta = function(v, a) {
	if( !a ) { a = 1 }
	var sigmoid = Activation.sigmoid(v, a);
	return ( a * ( sigmoid ) * ( 1 - sigmoid ) );
};

// Hyperbolic Tangent
Activation.hypertangent = function(v, b) {
	if( !b ) { b = 1 }
	var big = Math.exp(b * v);
	big = ( isFinite(big) ? big : Math.pow(10,100) );
	var small = Math.exp(-b * v);
	small = ( isFinite(small) ? small : Math.pow(10,100) );
	var value = ((big - small) / (big + small));
	return value;
};

Activation.hypertangent.delta = function (v, b) {
	if( !b ) { b = 1 }
	var hyper = Activation.hypertangent(v, b);
	return ( b * ( 1 - ( hyper * hyper ) ) );
};