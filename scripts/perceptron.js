/***************
 * JS-NeuralNet
 * Created by Sean Lander (c) 2012, All Rights Reserved
 
   This file is part of JS-NeuralNet.

   JS-NeuralNet is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   JS-NeuralNet is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with JS-NeuralNet.  If not, see <http://www.gnu.org/licenses/>.
 ***************/

/***************
 * Perceptron Properties:
 *  input_count: Number of input nodes coming in (not counting bias)
 *	randomize_weights: True or False
 *	default_weights: float value (0,1)
 *  activation: Activation Function
 *	alpha: Learning Rate
 *	beta: Momentum Term
 *	hidden: True or False
 *	v: Value of sum of weight*output from previous layer
 *	y: Ouput for current preceptron ( activation(v) )
 *	error: Error for current pattern
 *	gradient: Local Gradient for current pattern
 *
 * Perceptron Objects:
 *	weights: Array of weights from previous layer to this perceptron
 *	dweights: Array of changes to be made to the weights after full backpropagation
 *		is finished
 *
 * Perceptron Functions:
 *	randomizeWeights: Randomizes the weights between -1 and 1 to precision of 4
 *	run: Runs the perceptron using the provided input array
 ***************/


/***************
 * Constructor:
 * 	Inputs:
 *		input_count: Number of inputs from the previous layer (not counting bias)
 *		randomize_weights: True or False
 *		default_weights:	True or False
 *		activation: Activation function used (Activation object)
 *		hidden: True or False
 *	Output:
 *		self
 ***************/
function Perceptron(input_count,
		randomize_weights,
		default_weight,
		activation,
		alpha,
		beta,
		hidden) {
	this.input_count = input_count;
	this.weights = [];
	this.dweights = [];
	if( randomize_weights == true ) {
		this.randomizeWeights();
	} else if( 0 < default_weight && default_weight < 1 ) {
		for( var i = 0 ; i < input_count+1 ; i++ ) {
			this.weights[i] = default_weight;
			this.dweights[i] = 0;
		}
	} else {
		for( var i = 0 ; i < input_count+1 ; i++ ) {
			this.weights[i] = 0.1;
			this.dweights[i] = 0;
		}
	}
	this.activation = activation;
	this.alpha = alpha;
	this.beta = beta;
	this.hidden = hidden;
};


/***************
 * RandomizeWeights:
 *	Randomizes the weights between -1 and 1 to precision of 4
 * 	Inputs:
 *		none
 *	Output:
 *		self
 ***************/
Perceptron.prototype.randomizeWeights = function() {
	for( var i = 0 ; i < this.input_count + 1 ; i++ ) {
		this.weights[i] = parseFloat( ( ((Math.random()*10)) + (Math.random()) ).toFixed(2) );
		this.dweights[i] = 0;
	}
	return this;
};


/***************
 * Run:
 *	Takes in an array of values, calculates V (sum of input times weight)
 *	then calculates the perceptron's Y (activation function given V)
 *	and returns the perceptron.
 * 	Inputs:
 *		inputs: Values from previous layer
 *	Output:
 *		self
 ***************/
Perceptron.prototype.run = function(inputs) {
	return this.calculateV(inputs).calculateY();
};


/***************
 * GetWeight:
 *	Retrieve the specified weight (used for training)
 * 	Inputs:
 *		index: Index of the weight in question
 *	Output:
 *		weight
 ***************/
Perceptron.prototype.getWeight = function(index) {
	// This should be the weight from the perceptron with index 'index'
	// from the previous layer
	return this.weights[index];
};


/***************
 * GetWeights:
 *	Retrieve the current perceptron's weight array
 * 	Inputs:
 *		none
 *	Output:
 *		weights
 ***************/
Perceptron.prototype.getWeights = function() {
	return jQuery.extend([], this.weights);
};


/***************
 * Calculation and Getter Functions
 *
 * Calculate V:
 *	Takes inputs from previous layer and calculates the sum of each input
 *	multiplied against a weight, and stores it in the perceptron
 * 	Inputs:
 *		inputs: Value array from previous layer
 *	Output:
 *		self
 *
 * CalculateY:
 *	Uses currently set V and Activation function and calculates the new
 *	output value for the node.
 *	Inputs:
 *		none
 *	Output:
 *		self
 *
 * CalculateError:
 *	Take a desired output and calculate an error.
 *	Inputs:
 *		desired: Desired Value of output
 *	Output:
 *		self
 *
 * GetV:
 *	Return the current value of V
 *	Inputs:
 *		none
 *	Output:
 *		v
 *
 * GetY:
 *	Return the current value of Y
 *	Inputs:
 *		none
 *	Output:
 *		y
 *
 * GetError:
 *	Return the current value of Error
 *	Inputs:
 *		none
 *	Output:
 *		error
 ***************/
Perceptron.prototype.calculateV = function(inputs) {
	this.v = 0;
	for( var i = 0 ; i < this.input_count ; i++ ) {
		this.v += this.weights[i]*inputs[i];
	}
	this.v += this.weights[this.input_count];
	return this;
};

Perceptron.prototype.getV = function() {
	return this.v;
};

Perceptron.prototype.calculateY = function() {
	this.y = this.activation(this.v);
	return this;
};

Perceptron.prototype.getY = function() {
	return this.y;
};

Perceptron.prototype.calculateError = function(desired) {
	this.error = desired - this.getY();
	return this;
};

Perceptron.prototype.getError = function() {
	return this.error;
};


/***************
 * Backpropogation Methods
 *
 * CalculateGradient:
 *	Use the current error to calculate a local gradient (if outer layer)
 *	or the sum of the gradients from the next layer up.
 *	Inputs:
 *		gradientweightsum: Sum of the local gradients multiplied by their weights of
 *			future layers
 *	Output:
 *		self
 *
 * CalculateDWeights:
 *	Use alpha, beta, the local gradient and the previous weight deltas to calculate
 *	the new weight deltas
 *	Inputs:
 *		none
 *	Output:
 *		self
 *
 * GetGradient:
 *	Return the current local gradient
 *	Inputs:
 *		none
 *	Output:
 *		gradient
 *
 * GetDWeight:
 *	Returns the specified weight delta
 *	Inputs:
 *		index: Index of the weight delta to grab
 *	Output:
 *		dweight
 *
 * GetDWeights:
 *	Returns the array of weight deltas
 *	Inputs:
 *		none
 *	Output:
 *		dweights
 ***************/
Perceptron.prototype.calculateGradient = function(gradientweightsum) {
	if( this.hidden ) {
		this.gradient = this.activation.delta(this.v)*gradientweightsum;
	} else {
		this.gradient = this.error*this.activation.delta(this.v);
	}
	return this;
};

Perceptron.prototype.getGradient = function() {
	return this.gradient;
};

Perceptron.prototype.calculateDWeights = function(inputs) {
	for( var i = 0 ; i < this.input_count ; i++ ) {
		this.dweights[i] = this.alpha*this.gradient*inputs[i] + this.beta*this.dweights[i];
	}
	this.dweights[this.input_count] = this.alpha*this.gradient + this.beta*this.dweights[this.input_count];
	return this;
};

Perceptron.prototype.getDWeight = function(index) {
	return this.dweights[index];
};

Perceptron.prototype.getDWeights = function() {
	return jQuery.extend([], this.dweights);
};

Perceptron.prototype.applyDWeights = function() {
	for( var i = 0 ; i < this.input_count ; i++ ) {
		this.weights[i] += this.dweights[i];
	}
	this.weights[this.input_count] += this.dweights[this.input_count];
	return this;
};
