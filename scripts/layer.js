/***************
 * JS-NeuralNet
 * Created by Sean Lander (c) 2012, All Rights Reserved
 
   This file is part of JS-NeuralNet.

   JS-NeuralNet is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   JS-NeuralNet is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with JS-NeuralNet.  If not, see <http://www.gnu.org/licenses/>.
 ***************/

/***************
 * Perceptron Layer Properties:
 *	perceptron_count: Number of perceptrons in the layer (not counting bias)
 *	input_count: Number of inputs from the previous layer (not counting bias)
 *	randomize_weights: True or False
 *	default_weight: Float value (0,1)
 *	activation: Activation function used (Activation object)
 *	alpha: Learning Rate
 *	beta: Momentum Term
 *	hidden: True or False
 *
 * Perceptron Layer Objects:
 *	perceptrons: Array containing all perceptrons in the layer
 ***************/

/***************
 * Constructor:
 *	Input:
 *		perceptron_count
 *		input_count
 *		randomize_weights
 *		default_weight
 *		activation
 *		hidden
 ***************/
function Layer(perceptron_count,
	input_count,
	randomize_weights,
	default_weight,
	activation,
	alpha,
	beta,
	hidden) {
	
	// Initialize Values to Layer
	this.perceptron_count = perceptron_count;
	this.input_count = input_count;
	this.randomize_weights = (randomize_weights = true);
	this.default_weight = (default_weight ? default_weight : 0.1);
	this.activation = activation;
	this.alpha = alpha;
	this.beta = beta;
	this.hidden = (hidden == true);
	this.perceptrons = [];

	// Create Perceptrons
	for( var i = 0 ; i < perceptron_count ; i++ ) {
		this.perceptrons.push(new Perceptron(this.input_count,
																					this.randomize_weights,
																					this.default_weight,
																					this.activation,
																					this.alpha,
																					this.beta,
																					this.hidden));
	}
};


/***************
 * Run:
 *	Takes in an array of values and calculates the outputs for the
 *	perceptrons in this layer, returning the layer.
 * 	Inputs:
 *		inputs: Values from the previous layer
 *	Output:
 *		success: self
 *		failure: undefined
 ***************/
Layer.prototype.run = function(inputs) {
	// Validate that inputs.length is equal to input_count
	if( inputs.length != this.input_count ) {
		return undefined;
	}

	// Loop through and run each perceptron
	for( var i = 0 ; i < this.perceptrons.length ; i++ ) {
		this.perceptrons[i].run(inputs);
	}
	
	return this;
};


/***************
 * GetOutputs:
 *	Returns an array of output values for the layer's perceptrons
 * 	Inputs:
 *		none
 *	Output:
 *		outputs: Array of output values
 ***************/
Layer.prototype.getOutputs = function() {
	var outputs = [];
	for( var i = 0 ; i < this.perceptrons.length ; i++) {
		outputs.push(this.perceptrons[i].getY());
	}
	return outputs;
};


/***************
 * CalculateErrors:
 *	Calculate the errors for the layer's perceptrons
 * 	Inputs:
 *		desired
 *	Output:
 *		self
 ***************/
Layer.prototype.calculateErrors = function(desired) {
	for( var i = 0 ; i < this.perceptrons.length ; i++ ) {
		this.perceptrons[i].calculateError(desired[i]);
	}
	return this;
};


/***************
 * GetErrors:
 *	Returns an array of error values for the layer's perceptrons
 * 	Inputs:
 *		none
 *	Output:
 *		errors: Array of error values
 ***************/
Layer.prototype.getErrors = function() {
	var errors = [];
	for( var i = 0 ; i < this.perceptrons.length ; i++ ) {
		errors.push(this.perceptrons[i].getError());
	}
	return errors;
};


/***************
 * GetPerceptrons:
 *	Returns an array of the layer's perceptrons
 * 	Inputs:
 *		none
 *	Output:
 *		perceptrons: Array of preceptrons
 ***************/
Layer.prototype.getPerceptrons = function() {
	var perceptrons = [];
	for( var i = 0 ; i < this.perceptrons.length ; i++ ) {
		perceptrons.push(this.perceptrons[i]);
	}
	return perceptrons;
};


/***************
 * CalculateGradients:
 *	Calculate the gradients for the layer's perceptrons
 * 	Inputs:
 *		nextlayer: The next layer in the MLP, from which to take gradient and weight values
 *	Output:
 *		self
 ***************/
Layer.prototype.calculateGradients = function(nextlayer) {
	if( this.hidden ) {
		var gradients = nextlayer.getGradients();
		var perceptrons = nextlayer.getPerceptrons();
		var gradientweightsum = 0;
		for( var j = 0 ; j < this.perceptrons.length ; j++ ) {
			gradientweightsum = 0;
			for( var l = 0 ; l < perceptrons.length ; l++ ) {
				gradientweightsum += gradients[l]*perceptrons[l].getWeight(j);
			}
			this.perceptrons[j].calculateGradient(gradientweightsum);
		}
	} else {
		for( var l = 0 ; l < this.perceptrons.length ; l++ ) {
			this.perceptrons[l].calculateGradient();
		}
	}
	return this;
};


/***************
 * GetGradients:
 *	Returns an array of gradient values for the layer's perceptrons
 * 	Inputs:
 *		none
 *	Output:
 *		gradients: Array of error values
 ***************/
Layer.prototype.getGradients = function() {
	var gradients = [];
	for( var i = 0 ; i < this.perceptrons.length ; i++ ) {
		gradients.push(this.perceptrons[i].getGradient());
	}
	return gradients;
};


/***************
 * CalculateDweights:
 *	Calculates the weight delta for the layer's perceptrons
 * 	Inputs:
 *		none
 *	Output:
 *		self
 ***************/
Layer.prototype.calculateDWeights = function(inputs) {
	for( var i = 0 ; i < this.perceptrons.length ; i++ ) {
		this.perceptrons[i].calculateDWeights(inputs);
	}
	return this;
};


/***************
 * ApplyDWeights:
 *	Applies the new weight deltas to the layer's perceptrons
 * 	Inputs:
 *		none
 *	Output:
 *		self
 ***************/
Layer.prototype.applyDWeights = function() {
	for( var i = 0 ; i < this.perceptrons.length ; i++ ) {
		this.perceptrons[i].applyDWeights();
	}
	return this;
};
 